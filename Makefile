help:
	@echo "build - Build container"
	@echo "run - Run container"
	@echo "stop - Stop an already running container"
        
build:
	docker build -t="localareagames/lem" .
        
run:	build
	docker run -p 8081:80 -t -i -d -v $(CURDIR):/opt/lem localareagames/lem
	
stop:	
	docker ps | grep lem | awk '{ print $1 }' | xargs docker stop
